const baseUrl = "http://127.0.0.1:8001/";

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        // 'Access-Control-Allow-Origin': false
    }
});

$(function() {
    var table = $('.yajra-datatable1').DataTable({
        processing: true,
        serverSide: true,
        ajax: baseUrl + "student-list",
        columns: [{
                data: 'DT_RowIndex',
                name: 'DT_RowIndex'
            },
            {
                data: 'studentName',
                studentName: 'name'
            },
            {
                data: 'grade',
                grade: 'grade'
            },
            {
                data: 'image',
                Image: '<img src="/report/" + data + "" height="50"/>'
            },
            {
                data: 'dateOfBirth',
                Dateofbirth: 'dateofBirth'
            },
            {
                data: 'address',
                Address: 'address'
            },
            {
                data: 'action',
                name: 'action',
                orderable: true,
                searchable: true
            },
        ]
    });
});
$(document).ready(function(){
    $.ajax({
        url: '/country-list',
        type: "GET",
        dataType: 'json',
        success: function(result) {
            $.map(result.data, function(elementOfArray, indexInArray) {
                var country_name = elementOfArray['country_name'];
                $('#country').append('<option value="'+country_name+'">'+country_name+'</option>');
            });
        },
        error: function (xhr, ajaxOptions, thrownError) {
        console.log('status',xhr.status);
        console.log('error msg',xhr.responseText);
      }
    });
});
/*country select event*/
$('#country').on('change', function() {
    var country_id = this.value;
    // $("#state").html('');
    $.ajax({
        url: 'state-list/'+country_id,
        type: "GET",
        dataType: 'json',
        success: function(result) {
            $('#state').empty();
            $.map(result.data, function(elementOfArray, indexInArray) {
                var state_name = elementOfArray['state_name'];
                $('#state').append('<option value="'+state_name+'">'+state_name+'</option>');
            });
        },
        error: function(err){
            console.log('error => ', err);

        }
    });
});

/*state select event*/
$('#state').on('change', function() {
    var stateId = this.value;

    $.ajax({
        url: '/city-list/'+stateId,
        type: "GET",
        dataType: 'json',
        success: function(result) {
            $('#city').empty();
            $.map(result.data, function(elementOfArray, indexInArray) {
                var city_name = elementOfArray['city_name'];
                $('#city').append('<option value="'+city_name+'">'+city_name+'</option>');
            });
        },
        error: function(err){
            console.log('error => ', err);

        }
    });
});