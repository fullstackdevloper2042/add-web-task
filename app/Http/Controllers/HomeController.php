<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\State;
use App\Models\City;
use App\Models\Student;
use Http;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $totalStudent = Student::count();
        return view('home',compact('totalStudent'));
    }

    public function getCountries(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://www.universal-tutorial.com/api/countries',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJfZW1haWwiOiJmdWxsc3RhY2tkZXZsb3BlcjIwNDJAZ21haWwuY29tIiwiYXBpX3Rva2VuIjoiUU5JRnJqdk1JZmZsODVtRHBKX3o1bXVZbUQ0N0NKbG1YMzFBTm42ZlNJUV81cmFzUDlGMkNDWURNcGU0STM4anZZOCJ9LCJleHAiOjE2MjM1NjEyOTh9.r1khR_9PTbSZmbvrFdGH1CYum0GapvqDrPJzZeWJW2c',
            'Accept: application/json'
          ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $data = json_decode($response, true);
        // return $data['error'];
        if(!isset($data['error'])){
            return response()->json(['data'=>$data], 200);
        }
        return response()->json(['data'=>$data], 400);
    }
    public function getStateList($id){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://www.universal-tutorial.com/api/states/'.$id,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJfZW1haWwiOiJmdWxsc3RhY2tkZXZsb3BlcjIwNDJAZ21haWwuY29tIiwiYXBpX3Rva2VuIjoiUU5JRnJqdk1JZmZsODVtRHBKX3o1bXVZbUQ0N0NKbG1YMzFBTm42ZlNJUV81cmFzUDlGMkNDWURNcGU0STM4anZZOCJ9LCJleHAiOjE2MjM1NjEyOTh9.r1khR_9PTbSZmbvrFdGH1CYum0GapvqDrPJzZeWJW2c',
            'Accept: application/json'
          ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $data = json_decode($response, true);

        if(!isset($data['error'])){
            return response()->json(['data'=>$data], 200);
        }
        return response()->json(['data'=>$data], 400);
        /*$state = State::where('countryId', $id)->get();
        return response()->json($state);*/
    }

    public function getCityList($id){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://www.universal-tutorial.com/api/cities/'.$id,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJfZW1haWwiOiJmdWxsc3RhY2tkZXZsb3BlcjIwNDJAZ21haWwuY29tIiwiYXBpX3Rva2VuIjoiUU5JRnJqdk1JZmZsODVtRHBKX3o1bXVZbUQ0N0NKbG1YMzFBTm42ZlNJUV81cmFzUDlGMkNDWURNcGU0STM4anZZOCJ9LCJleHAiOjE2MjM1NjEyOTh9.r1khR_9PTbSZmbvrFdGH1CYum0GapvqDrPJzZeWJW2c',
            'Accept: application/json'
          ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $data = json_decode($response, true);

        if(!isset($data['error'])){
            return response()->json(['data'=>$data], 200);
        }
        return response()->json(['data'=>$data], 400);
        /*$city = City::where('stateId', $id)->get();
        return response()->json($city);*/
    }
}
